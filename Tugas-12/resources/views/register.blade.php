<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Page Tugas 1</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label>First Name: </label><br><br>
        <input type="text" name="fname"><br><br>

        <label>Last Name: </label><br><br>
        <input type="text" name="lname"><br><br>

        <label>Gender: </label><br><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br>
        <input type="radio" name="gender">Other<br><br>

        <label>Nationality: </label><br>
        <select name="nationality"><br>
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Philliphines">Philliphines</option>
            <option value="Australian">Australian</option>
        </select><br><br>

        <label>Language Spoken</label><br>
        <input type="checkbox" name="language"> Bahasa Indonesia<br>
        <input type="checkbox" name="language"> English<br>
        <input type="checkbox" name="language"> Other<br><br>

        <label>Bio</label><br>
        <textarea rows="10" cols="20"></textarea><br><br>

        <input type="submit" value="Sign Up"><br>
    </form>
</body>
</html>